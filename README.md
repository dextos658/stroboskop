# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git init
git remote add origin https://dextos658@bitbucket.org/dextos658/stroboskop.git
git pull origin master
```

Naloga 6.2.3:
https://bitbucket.org/dextos658/stroboskop/commits/6f3d2a9c5741d9bfdcc902a2217ee346494ecc8f

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/dextos658/stroboskop/commits/327140d702743089c1483457f1544fdedaa0160e

Naloga 6.3.2:
https://bitbucket.org/dextos658/stroboskop/commits/da4649535bb955674b3ddf9fdf7c658fb978a5af

Naloga 6.3.3:
https://bitbucket.org/dextos658/stroboskop/commits/0a8d22a2eaa2e2781ad20bec591c2a54440842d6

Naloga 6.3.4:
https://bitbucket.org/dextos658/stroboskop/commits/8339dc2ee2e644fc755c987d42582b76adc65e5d

Naloga 6.3.5:

```
git checkout master
git merge izgled --no-ff
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/dextos658/stroboskop/commits/97fb831c59613744a6fbaac9a120228a92c7e874

Naloga 6.4.2:
https://bitbucket.org/dextos658/stroboskop/commits/632936a80286fec3c90f69239adeafff7852ba19

Naloga 6.4.3:
https://bitbucket.org/dextos658/stroboskop/commits/dd5194556f6d7893dcc8305f06f0003f0de723bf

Naloga 6.4.4:
https://bitbucket.org/dextos658/stroboskop/commits/51402304a93b48d3b3c47f3ef2294962cc6e20ee